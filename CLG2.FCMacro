import FreeCAD
import FreeCADGui
import os
import csv
from collections import defaultdict
from PySide2 import QtWidgets
import re
# Cut list generator created with the help of ChatGPT
# If there is an assembly in the FreeCAD file it will process the linked objects in the assembly and produce a cut list.
# If there no assembly it will add all the bodies in the FreeCAD file to the cut list.
# The file is called CLG2 (Cut List Generator 2) it will save the cut list with same name as the FreeCAD file with a .CSV extension
# in the same folder that the FreeCAD file is saved in.
# Always save your file before you run the Macro.
#
#  Author: Adventures in Creation 12/2024

# Function to get sorted dimensions of a Body (returns Length, Width, Thickness)
def get_dimensions(body):
    try:
        # Check if the body has a shape (avoid errors for linked objects)
        if hasattr(body, 'Shape') and body.Shape:
            bounding_box = body.Shape.BoundBox
            dimensions = sorted(
                [bounding_box.XLength, bounding_box.YLength, bounding_box.ZLength],
                reverse=True
            )
            return dimensions[0], dimensions[1], dimensions[2]
        else:
            return None
    except Exception as e:
        print(f"Error getting dimensions for {body.Label}: {e}")
        return None

# Function to extract the base name of a body (remove numerical suffix)
def get_base_name(label):
    return re.sub(r'\d+$', '', label)

# Function to traverse the document and find all linked objects
def gather_linked_objects(doc):
    linked_bodies_count = defaultdict(int)  # Will store counts of bodies being linked
    
    for obj in doc.Objects:
        if obj.TypeId == "App::Link" and obj.LinkedObject:
            linked_obj = obj.LinkedObject
            if linked_obj.TypeId == "PartDesign::Body":
                # Increment count for this body each time it's linked
                linked_bodies_count[linked_obj.Label] += 1
                print(f"Found Linked Object: {obj.Label} linked to Body: {linked_obj.Label}")

    return linked_bodies_count

# Function to generate the cut list (with quantities)
def generate_cut_list(doc):
    cut_list = defaultdict(lambda: {"Name": "", "Length": 0, "Width": 0, "Thickness": 0, "Quantity": 0})

    # Get linked objects and count the occurrences for each body
    linked_bodies_count = gather_linked_objects(doc)

    if not linked_bodies_count:
        print("No linked bodies found in the assembly.")
        # If no linked objects found, process bodies in the part (not an assembly)
        print("Processing bodies directly in the part...")
        for obj in doc.Objects:
            if obj.TypeId == "PartDesign::Body":
                body = obj
                dimensions = get_dimensions(body)
                if dimensions:
                    base_name = get_base_name(body.Label)
                    key = (base_name, *dimensions)  # Unique key: (Name, Length, Width, Thickness)
                    cut_list[key]["Name"] = base_name
                    cut_list[key]["Length"] = dimensions[0]
                    cut_list[key]["Width"] = dimensions[1]
                    cut_list[key]["Thickness"] = dimensions[2]
                    cut_list[key]["Quantity"] += 1  # Quantity is 1 since it's a single body in the part
    else:
        # For each body in the linked count, get its dimensions and create the cut list
        for body_label, quantity in linked_bodies_count.items():
            body = doc.getObject(body_label)
            dimensions = get_dimensions(body)

            if dimensions:
                base_name = get_base_name(body.Label)
                key = (base_name, *dimensions)  # Unique key: (Name, Length, Width, Thickness)
                cut_list[key]["Name"] = base_name
                cut_list[key]["Length"] = dimensions[0]
                cut_list[key]["Width"] = dimensions[1]
                cut_list[key]["Thickness"] = dimensions[2]
                cut_list[key]["Quantity"] += quantity  # Add to quantity

    return list(cut_list.values())

# Function to save the cut list to a CSV file
def save_cut_list_to_csv(cut_list, output_file):
    try:
        with open(output_file, 'w', newline='') as csvfile:
            fieldnames = ["Name", "Length", "Width", "Thickness", "Quantity"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for item in cut_list:
                writer.writerow(item)
        return True, output_file
    except Exception as e:
        return False, str(e)

# Function to show popup messages
def show_popup(message, title="Message"):
    try:
        QtWidgets.QMessageBox.information(None, title, message)
    except Exception:
        print(f"{title}: {message}")

# Main execution
if __name__ == "__main__":
    doc = FreeCAD.ActiveDocument
    if doc is None:
        show_popup("No active document found. Please open a FreeCAD document.", "Error")
    else:
        # Generate output file path
        freecad_file = doc.FileName
        if not freecad_file:
            show_popup("The FreeCAD document must be saved first.", "Error")
        else:
            output_file = os.path.splitext(freecad_file)[0] + ".csv"

            # Generate and save the cut list
            try:
                cut_list = generate_cut_list(doc)
                if not cut_list:
                    show_popup("The cut list is empty. Please check the document and linked bodies.", "Error")
                else:
                    success, result = save_cut_list_to_csv(cut_list, output_file)

                    if success:
                        show_popup(f"Cut list created successfully:\n{result}", "Success")
                    else:
                        show_popup(f"Failed to save cut list:\n{result}", "Error")
            except Exception as e:
                show_popup(f"An unexpected error occurred:\n{str(e)}", "Error")
