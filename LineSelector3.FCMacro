# LineSelector3 a simple macro to allow you to select lines by type
# Written by Adventures in Creation with assistance from chatGPT
# 01/16/2025
import FreeCAD
import FreeCADGui
from PySide2 import QtWidgets

class LineSelectorDialog(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()

        # Set up the dialog window
        self.setWindowTitle("Select Line Type")
        self.setGeometry(300, 300, 250, 150)  # Adjust the position here if needed

        # Create a layout for the window
        layout = QtWidgets.QVBoxLayout()

        # Add a combo box to choose the line type
        self.line_type_combo = QtWidgets.QComboBox()
        self.line_type_combo.addItem("B-spline")
        self.line_type_combo.addItem("Construction Line")
        self.line_type_combo.addItem("Line")
        self.line_type_combo.addItem("Arc")
        self.line_type_combo.addItem("Circle")  # Added "Circle"
        self.line_type_combo.addItem("None")    # Added "None" for deselecting

        layout.addWidget(self.line_type_combo)

        # Add checkboxes
        self.add_to_selection_checkbox = QtWidgets.QCheckBox("Add to current selection")
        layout.addWidget(self.add_to_selection_checkbox)

        self.exclude_checkbox = QtWidgets.QCheckBox("Select everything but")
        layout.addWidget(self.exclude_checkbox)

        # Add a "Select" button
        self.select_button = QtWidgets.QPushButton("Select")
        self.select_button.clicked.connect(
            lambda: select_lines_by_type(
                self.line_type_combo.currentText(),
                self.add_to_selection_checkbox.isChecked(),
                self.exclude_checkbox.isChecked(),
            )
        )
        layout.addWidget(self.select_button)

        # Set the layout for the dialog
        self.setLayout(layout)

    def show(self):
        # Make sure the dialog stays non-modal
        super().show()

# Persistent reference to the dialog
dialog = None

def show_gui():
    global dialog
    if dialog is None:  # Check if dialog already exists
        dialog = LineSelectorDialog()
    dialog.show()  # Show the dialog as a non-modal window

# Function to select lines by type with the option to add to current selection or exclude
def select_lines_by_type(line_type, add_to_selection=False, exclude=False):
    """
    Select lines of a specific type in the active sketch. Optionally add to current selection or exclude.
    """
    # Attempt to get the object currently being edited
    edit_object = FreeCADGui.ActiveDocument.getInEdit()

    if hasattr(edit_object, "Object"):
        active_sketch = edit_object.Object
    else:
        active_sketch = edit_object

    # Verify that the resolved object is a sketch
    if not active_sketch or active_sketch.TypeId != "Sketcher::SketchObject":
        print("No active sketch found. Please ensure a sketch is open and in edit mode.")
        return

    print(f"Processing sketch: {active_sketch.Name}")

    # List to store indices of the selected lines
    selected_indices = []

    # Iterate through the geometry of the sketch
    for index, geom in enumerate(active_sketch.Geometry):
        print(f"Checking geometry {index}: {geom.TypeId}")

        if line_type == "B-spline" and geom.TypeId == "Part::GeomBSplineCurve":
            selected_indices.append(index)
        elif line_type == "Line" and geom.TypeId == "Part::GeomLineSegment":
            if not active_sketch.getConstruction(index):
                selected_indices.append(index)
        elif line_type == "Arc" and geom.TypeId == "Part::GeomArcOfCircle":
            if not active_sketch.getConstruction(index):
                selected_indices.append(index)
        elif line_type == "Circle" and geom.TypeId == "Part::GeomCircle":  # Handle circles
            if not active_sketch.getConstruction(index):
                selected_indices.append(index)
        elif line_type == "Construction Line" and active_sketch.getConstruction(index):
            selected_indices.append(index)

    # Apply selection logic (include, exclude, or deselect)
    if line_type == "None":
        # Deselect everything
        FreeCADGui.Selection.clearSelection()
        print("Deselected all lines.")
    elif exclude:
        # Exclude selected type
        current_selection = [f"Edge{i + 1}" for i in range(len(active_sketch.Geometry))]
        excluded_selection = [f"Edge{i + 1}" for i in selected_indices]
        final_selection = set(current_selection) - set(excluded_selection)

        FreeCADGui.Selection.clearSelection()
        for edge in final_selection:
            FreeCADGui.Selection.addSelection(active_sketch, edge)

        print(f"Excluded {len(selected_indices)} {line_type}(s) from the selection.")
    elif add_to_selection:
        # Add to the current selection
        for index in selected_indices:
            FreeCADGui.Selection.addSelection(active_sketch, f"Edge{index + 1}")
        print(f"Added {len(selected_indices)} {line_type}(s) to the selection.")
    else:
        # Replace the current selection
        FreeCADGui.Selection.clearSelection()
        for index in selected_indices:
            FreeCADGui.Selection.addSelection(active_sketch, f"Edge{index + 1}")
        print(f"Selected {len(selected_indices)} {line_type}(s) in the sketch.")

# Run the GUI
show_gui()
