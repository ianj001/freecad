# README #

This is a repository for useful FreeCAD stuff

### What is this repository for? ###

* Macros
* How-Tos
* CAD files
* Other files to share with the community


### How do I get set up? ###

* You can git clone this repository
* Or download what you want
* You will need FreeCAD to be able to use the stuff here


### Contribution guidelines ###

* If you want to modify and update the files please use the git repository to do so.


### Who do I talk to? ###

* ianj001 on the forums
* Adventures in Creation on YouTube https://www.youtube.com/user/ianj001/